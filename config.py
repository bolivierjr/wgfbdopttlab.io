#!/user/bin/env python

'''
To send an email if any errors occur. Fill out with the correct info.
'''
SMTP = {'host': 'smtp.example.com',
        'port': '25',
        'from': 'APPLICATION ALERT <noreply@example.com>',
        'to': 'example@gmail.com',
        'subject': 'WGFBDOPTT Error'}
