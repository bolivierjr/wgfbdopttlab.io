#!/usr/bin/env python
'''
Process a team schedule from mlb.com to be used as backend data
for willgiantsfansbedicksonpublictransittoday.com.

Outputs a json document by default to public/data/giants<YEAR>schedule.json.

Takes the following args:
    output-file:    the file to write the json to
    url:            the url to pull the schedule from
'''
import sys
from argparse import ArgumentParser
import csv
import datetime
import json
import logging
from logging.handlers import SMTPHandler
from config import SMTP
import requests

CSV_URL = 'http://www.ticketing-client.com/ticketing-client/csv/EventTicketPromotionPrice.tiksrv'  # pylint: disable=C0301
OUTFILE = 'public/data/giants{}schedule.json'.format(datetime.datetime.now().year)


def getcsv(url, logger):
    '''
    Given a url to a csv, pull, decode and return the csv.
    '''
    payload = {'team_id': '137', 'display_in': 'singlegame',
               'ticket_category': 'Tickets', 'site_section': 'Default',
               'sub_category': 'Default', 'leave_empty_games': 'true',
               'event_type': 'T'}

    try:
        with requests.Session() as session:
            download = session.get(url, params=payload, timeout=30)
            download.raise_for_status()
            decoded_content = download.content.decode('utf-8')
            return decoded_content

    except requests.exceptions.HTTPError:
        logger.exception('Unhandled HTTP Exception')
        sys.exit(1)

    except requests.exceptions.ConnectionError:
        logger.exception('Unhandled Connection Exception')
        sys.exit(1)

    except requests.exceptions.Timeout:
        logger.exception('Unhandled Timeout Exception')
        sys.exit(1)


def process_schedule(csvdata):
    '''
    Given an mlb schedule csv, return the date, opponent, time and location
    fields in a json document.
    '''
    raw_schedule = csv.reader(csvdata.splitlines(), delimiter=',')
    schedule = []

    # Skip header row
    next(raw_schedule)

    # This CSV has 17 fields for each game, but we only want the following
    # four fields, which will look like this:

    # "date": "4/6/2012"        row[0]
    # "opponent": "D-backs"     row[3]
    # "time": "4:10pm"          row[1]
    # "location":"Chase Field"  row[4]

    for row in raw_schedule:
        # CSV uses 2-digit years; we want 4 digits in the JSON.
        date = ''.join([row[0][:6], '20', row[0][-2:]])

        # Remove the space and make "pm" lowercase
        time = ''.join(c.lower() for c in row[1] if not c.isspace())

        # Remove leading zeroes from times, such as 02:00pm
        if time and int(time[0]) == 0:
            time = time[1:]

        # Trim the subject description down to the opponent name
        opponent = row[3]
        opponent = opponent.replace('at', '').replace('Giants', '').strip()

        json_data = {"date": date,
                     "opponent": opponent,
                     "time": time,
                     "location": row[4]}
        schedule.append(json_data)

    full_contents = {"title": "Giants Game Schedule",
                     "link": "http://willgiantsfansbedicksonpublictransittoday.com/",
                     "games": schedule}

    return full_contents


def output_json(jsondata, outfile):
    '''
    Write given json data to the specified output file.
    '''
    with open(outfile, 'w') as writefile:
        writefile.write(json.dumps(jsondata, sort_keys=True, indent=4))


def main():  # pylint: disable=C0111
    parser = ArgumentParser(description='Output an MLB schedule as JSON.')
    parser.add_argument('-o', '--output-file',
                        dest='outfile',
                        help="file to write to",
                        default=OUTFILE)
    parser.add_argument('-u', '--url',
                        dest='url',
                        help="url to pull schedule from",
                        default=CSV_URL)

    args = parser.parse_args()

    smtp_handler = SMTPHandler(mailhost=(SMTP.get('host'), SMTP.get('port')),
                               fromaddr=SMTP.get('from'),
                               toaddrs=SMTP.get('to'),
                               subject=SMTP.get('subject'))

    logger = logging.getLogger()
    logger.addHandler(smtp_handler)

    try:
        csvschedule = getcsv(args.url, logger)
        jsonschedule = process_schedule(csvschedule)
        output_json(jsonschedule, args.outfile)

    except IOError:
        logger.exception('Unhandled Exception')
        sys.exit(1)


if __name__ == '__main__':
    main()
